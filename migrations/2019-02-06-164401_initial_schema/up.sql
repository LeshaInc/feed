create table users (
    id          serial primary key,
    username    text not null,
    created     timestamp not null default (now() at time zone 'utc')
);

create table credentials (
    user_id     integer primary key references users(id) on delete cascade,
    password    bytea not null,
    salt        bytea not null
);

create table posts (
    id      serial primary key,
    user_id integer not null references users(id),
    body    text not null,
    created timestamp not null default (now() at time zone 'utc')
);

create table likes (
    id      serial primary key,
    user_id integer not null references users(id),
    post_id integer not null references posts(id),
    created timestamp not null default (now() at time zone 'utc')
);

create table comments (
    id      serial primary key,
    user_id integer not null references users(id),
    post_id integer not null references posts(id),
    body    text not null,
    created timestamp not null default (now() at time zone 'utc')
);
