use actix_web::{Error, HttpRequest, HttpResponse, Json};
use diesel::prelude::*;
use futures::{future, Future};
use serde_derive::Deserialize;

use crate::app::{ApiResponse, AppState};
use crate::db;
use crate::error::{ApiError, ApiResultExt};
use crate::models::*;
use crate::util::AuthToken;

#[derive(Debug, Deserialize)]
pub struct MakePost {
    pub body: String,
}

pub fn make_post(
    (req, data): (HttpRequest<AppState>, Json<MakePost>),
) -> Box<dyn Future<Item = HttpResponse, Error = Error>> {
    let id = match req.extensions().get::<AuthToken>() {
        Some(v) => v.id,
        None => return Box::new(future::err(ApiError::Unauthenticated.into())),
    };

    let data = data.into_inner();
    let post = db::execute(&req.state().db, move |conn| {
        use crate::schema::posts::dsl as posts;

        if data.body.is_empty() || data.body.as_bytes().len() > 32_768 {
            return Err(ApiError::PostTooLong.into());
        }

        let new_post = NewPost {
            user_id: id,
            body: &data.body,
        };

        let post = diesel::insert_into(posts::posts)
            .values(&new_post)
            .get_result::<Post>(&conn)
            .internal_error()?;

        Ok(post)
    });

    let fut = post.map(|t| HttpResponse::Created().json(ApiResponse::ok(t)));
    Box::new(fut)
}
