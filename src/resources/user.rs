use actix_web::{Error, HttpResponse, Json, State};
use chrono::{Duration, Utc};
use diesel::prelude::*;
use futures::Future;
use serde_derive::Deserialize;

use crate::app::{ApiResponse, AppState};
use crate::db;
use crate::error::{ApiError, ApiResultExt};
use crate::models::*;
use crate::util::{generate_salt, hash_password, AuthToken};

#[derive(Debug, Deserialize)]
pub struct RegisterUser {
    pub username: String,
    pub password: String,
}

pub fn register(
    (state, data): (State<AppState>, Json<RegisterUser>),
) -> impl Future<Item = HttpResponse, Error = Error> {
    let data = data.into_inner();

    let user = db::execute(&state.db, move |conn| {
        use crate::schema::credentials::dsl as credentials;
        use crate::schema::users::dsl as users;

        if data.password.is_empty() || data.password.as_bytes().len() > 72 {
            return Err(ApiError::InvalidPassword.into());
        }

        if !USERNAME_RE.is_match(&data.username) {
            return Err(ApiError::InvalidUsername.into());
        }

        let exists = users::users
            .filter(users::username.eq(&data.username))
            .count()
            .get_result::<i64>(&conn)
            .internal_error()?
            > 0;

        if exists {
            return Err(ApiError::UsernameTaken.into());
        }

        let new_user = NewUser {
            username: &data.username,
        };

        let user = diesel::insert_into(users::users)
            .values(&new_user)
            .get_result::<User>(&conn)
            .internal_error()?;

        let salt = generate_salt();
        let password = hash_password(&salt, &data.password);

        let new_credential = NewCredential {
            user_id: user.id,
            password: &password,
            salt: &salt,
        };

        diesel::insert_into(credentials::credentials)
            .values(&new_credential)
            .execute(&conn)
            .internal_error()?;

        Ok(user)
    });

    user.map(|user| HttpResponse::Created().json(ApiResponse::ok(user)))
}

#[derive(Debug, Deserialize)]
pub struct Login {
    pub username: String,
    pub password: String,
}

pub fn login(
    (state, data): (State<AppState>, Json<Login>),
) -> impl Future<Item = HttpResponse, Error = Error> {
    let config = state.config.clone();
    let data = data.into_inner();
    let token = db::execute(&state.db, move |conn| {
        use crate::schema::users::dsl as users;

        if data.password.is_empty() || data.password.as_bytes().len() > 72 {
            return Err(ApiError::InvalidCredentials.into());
        }

        let user: User = users::users
            .filter(users::username.eq(&data.username))
            .get_result(&conn)
            .optional()
            .internal_error()?
            .ok_or(ApiError::InvalidCredentials)?;

        let credentials = Credential::belonging_to(&user)
            .get_result::<Credential>(&conn)
            .optional()
            .internal_error()?
            .ok_or(ApiError::InvalidCredentials)?;

        let hash = hash_password(&credentials.salt, &data.password);

        if &hash[..] != credentials.password.as_slice() {
            return Err(ApiError::InvalidCredentials.into());
        }

        let lifetime = Duration::from_std(config.auth.token_lifetime).internal_error()?;

        let token = AuthToken {
            iss: config.auth.iss.clone(),
            sub: data.username,
            exp: (Utc::now() + lifetime).timestamp(),
            id: user.id,
        };

        let token =
            jsonwebtoken::encode(&Default::default(), &token, config.auth.secret.as_bytes())
                .internal_error()?;

        Ok(token)
    });

    token.map(|t| HttpResponse::Ok().json(ApiResponse::ok(t)))
}
