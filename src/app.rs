use actix::prelude::*;
use actix_web::{http::Method, App};
use serde_derive::Serialize;

use crate::config::Config;
use crate::db::DbExecutor;
use crate::middlewares;
use crate::resources;

#[derive(Clone)]
pub struct AppState {
    pub db: Addr<DbExecutor>,
    pub config: Config,
}

#[derive(Serialize)]
pub struct ApiResponse<T> {
    pub success: bool,
    pub data: T,
}

impl<T> ApiResponse<T> {
    pub fn ok(data: T) -> ApiResponse<T> {
        ApiResponse {
            success: true,
            data,
        }
    }
}

pub fn create(state: AppState) -> App<AppState> {
    App::with_state(state)
        .middleware(middlewares::AuthMiddleware)
        .resource("/api/user", |r| {
            r.method(Method::POST).with_async(resources::user::register)
        })
        .resource("/api/login", |r| {
            r.method(Method::POST).with_async(resources::user::login)
        })
        .resource("/api/post", |r| {
            r.method(Method::POST)
                .with_async(resources::post::make_post)
        })
}
