use actix_web::http::header;
use actix_web::middleware::{Middleware, Started};
use actix_web::{HttpRequest, Result};

use crate::app::AppState;
use crate::error::ApiError;
use crate::util::AuthToken;

pub struct AuthMiddleware;

impl Middleware<AppState> for AuthMiddleware {
    fn start(&self, req: &HttpRequest<AppState>) -> Result<Started> {
        use jsonwebtoken::errors::ErrorKind;

        let bearer = req
            .headers()
            .get(header::AUTHORIZATION)
            .and_then(|header| header.to_str().ok())
            .map(|header| header.split_whitespace())
            .and_then(|mut parts| {
                if parts.next() == Some("Bearer") {
                    parts.next()
                } else {
                    None
                }
            });

        let bearer = match bearer {
            Some(v) => v,
            None => return Ok(Started::Done),
        };

        let config = &req.state().config;

        let token = jsonwebtoken::decode::<AuthToken>(
            bearer,
            config.auth.secret.as_bytes(),
            &Default::default(),
        );

        let token = match token {
            Err(e) => {
                let e = match e.kind() {
                    ErrorKind::ExpiredSignature => ApiError::TokenExpired,
                    _ => ApiError::InvalidToken,
                };

                return Err(e.into());
            }

            Ok(ref v) if v.claims.iss != config.auth.iss => {
                return Err(ApiError::InvalidToken.into());
            }

            Ok(v) => v,
        };

        req.extensions_mut().insert(token.claims);

        Ok(Started::Done)
    }
}
