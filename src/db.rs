use actix::prelude::*;
use actix_web::{Error, Result};
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use diesel::{Connection, PgConnection};
use failure::Fallible;
use futures::Future;

use crate::config::Config;
use crate::error::ApiResultExt;

#[derive(Clone)]
pub struct DbExecutor {
    pub pool: Pool<ConnectionManager<PgConnection>>,
    pub config: Config,
}

impl DbExecutor {
    pub fn new(c: &Config) -> Fallible<DbExecutor> {
        let connection = ConnectionManager::new(c.database.url.clone());
        let pool = Pool::builder()
            .max_size(c.database.max_size)
            .min_idle(c.database.min_idle)
            .max_lifetime(c.database.max_lifetime)
            .idle_timeout(c.database.idle_timeout)
            .connection_timeout(c.database.connection_timeout)
            .build(connection)?;

        Ok(DbExecutor {
            pool,
            config: c.clone(),
        })
    }
}

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub struct DbTask<F, C, T>
where
    F: FnOnce(C) -> Result<T>,
    C: Connection,
{
    f: F,
    _marker: std::marker::PhantomData<(C, T)>,
}

impl<F, C, T: 'static> Message for DbTask<F, C, T>
where
    F: FnOnce(C) -> Result<T>,
    C: Connection,
{
    type Result = Result<T>;
}

type Conn = PooledConnection<ConnectionManager<PgConnection>>;

impl<F, T: 'static> Handler<DbTask<F, Conn, T>> for DbExecutor
where
    F: FnOnce(Conn) -> Result<T>,
{
    type Result = Result<T>;

    fn handle(&mut self, msg: DbTask<F, Conn, T>, _: &mut Self::Context) -> Self::Result {
        let conn = self.pool.get().internal_error()?;

        (msg.f)(conn)
    }
}

pub fn execute<F, T>(db: &Addr<DbExecutor>, f: F) -> impl Future<Item = T, Error = Error>
where
    F: FnOnce(Conn) -> Result<T> + Send + 'static,
    T: Send + 'static,
{
    db.send(DbTask {
        f,
        _marker: Default::default(),
    })
    .from_err()
    .and_then(|v| v.map_err(From::from))
}
