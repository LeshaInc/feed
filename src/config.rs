use std::net::SocketAddr;
use std::time::Duration;

use failure::Fallible;
use serde::{Deserialize, Deserializer};
use serde_derive::Deserialize;

fn de_duration<'de, D>(deserializer: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let s = <&str>::deserialize(deserializer)?;
    parse_duration::parse(s).map_err(serde::de::Error::custom)
}

fn de_option_duration<'de, D>(deserializer: D) -> Result<Option<Duration>, D::Error>
where
    D: Deserializer<'de>,
{
    de_duration(deserializer).map(Some)
}

#[derive(Debug, Clone, Deserialize)]
pub struct DbConfig {
    pub url: String,
    pub max_size: u32,
    pub min_idle: Option<u32>,

    #[serde(default, deserialize_with = "de_option_duration")]
    pub max_lifetime: Option<Duration>,

    #[serde(default, deserialize_with = "de_option_duration")]
    pub idle_timeout: Option<Duration>,

    #[serde(deserialize_with = "de_duration")]
    pub connection_timeout: Duration,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AuthConfig {
    pub iss: String,
    pub secret: String,

    #[serde(deserialize_with = "de_duration")]
    pub token_lifetime: Duration,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    pub server_addr: SocketAddr,
    pub auth: AuthConfig,
    pub database: DbConfig,
}

pub fn load() -> Fallible<Config> {
    let contents = std::fs::read_to_string("config.toml")?;
    let config = toml::from_str(&contents)?;

    Ok(config)
}
