table! {
    comments (id) {
        id -> Int4,
        user_id -> Int4,
        post_id -> Int4,
        body -> Text,
        created -> Timestamp,
    }
}

table! {
    credentials (user_id) {
        user_id -> Int4,
        password -> Bytea,
        salt -> Bytea,
    }
}

table! {
    likes (id) {
        id -> Int4,
        user_id -> Int4,
        post_id -> Int4,
        created -> Timestamp,
    }
}

table! {
    posts (id) {
        id -> Int4,
        user_id -> Int4,
        body -> Text,
        created -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Text,
        created -> Timestamp,
    }
}

joinable!(comments -> posts (post_id));
joinable!(comments -> users (user_id));
joinable!(credentials -> users (user_id));
joinable!(likes -> posts (post_id));
joinable!(likes -> users (user_id));
joinable!(posts -> users (user_id));

allow_tables_to_appear_in_same_query!(comments, credentials, likes, posts, users,);
