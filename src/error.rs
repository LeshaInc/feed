use std::fmt::{Debug, Display};

use actix_web::error::ResponseError;
use actix_web::http::StatusCode;
use actix_web::HttpResponse;
use failure::Fail;
use serde_derive::Serialize;
use strum_macros::IntoStaticStr;

use crate::app::ApiResponse;

#[derive(Debug, Fail, IntoStaticStr)]
pub enum ApiError {
    #[fail(display = "username is already taken")]
    UsernameTaken,

    #[fail(display = "invalid username")]
    InvalidUsername,

    #[fail(display = "invalid password")]
    InvalidPassword,

    #[fail(display = "invalid credentials (either username or password)")]
    InvalidCredentials,

    #[fail(display = "invalid auth token")]
    InvalidToken,

    #[fail(display = "auth token expired")]
    TokenExpired,

    #[fail(display = "post is too long (32 kiB max)")]
    PostTooLong,

    #[fail(display = "this action requires authentication")]
    Unauthenticated,
}

impl ApiError {
    pub fn status_code(&self) -> StatusCode {
        match self {
            ApiError::UsernameTaken => StatusCode::CONFLICT,
            ApiError::InvalidUsername => StatusCode::BAD_REQUEST,
            ApiError::InvalidPassword => StatusCode::BAD_REQUEST,
            ApiError::InvalidCredentials => StatusCode::UNAUTHORIZED,
            ApiError::InvalidToken => StatusCode::UNAUTHORIZED,
            ApiError::TokenExpired => StatusCode::UNAUTHORIZED,
            ApiError::PostTooLong => StatusCode::PAYLOAD_TOO_LARGE,
            ApiError::Unauthenticated => StatusCode::UNAUTHORIZED,
        }
    }
}

#[derive(Serialize)]
struct ErrorPayload {
    status: u16,
    error_code: &'static str,
    message: String,
}

#[derive(Debug, Fail)]
pub struct InternalApiError<T>(pub T)
where
    T: Send + Sync + Debug + Display + 'static;

impl<T> Display for InternalApiError<T>
where
    T: Send + Sync + Debug + Display + 'static,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<T> ResponseError for InternalApiError<T>
where
    T: Send + Sync + Debug + Display + 'static,
{
    fn error_response(&self) -> HttpResponse {
        let body = ApiResponse {
            success: false,
            data: ErrorPayload {
                status: 500,
                error_code: "Internal",
                message: format!("{}", self),
            },
        };

        HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).json(body)
    }
}

impl ResponseError for ApiError {
    fn error_response(&self) -> HttpResponse {
        let body = ApiResponse {
            success: false,
            data: ErrorPayload {
                status: self.status_code().as_u16(),
                error_code: self.into(),
                message: format!("{}", self),
            },
        };

        HttpResponse::build(self.status_code()).json(body)
    }
}

pub trait ApiResultExt<T, E>
where
    E: Send + Sync + Debug + Display + 'static,
{
    fn internal_error(self) -> Result<T, InternalApiError<E>>;
}

impl<T, E> ApiResultExt<T, E> for Result<T, E>
where
    E: Send + Sync + Debug + Display + 'static,
{
    fn internal_error(self) -> Result<T, InternalApiError<E>> {
        self.map_err(InternalApiError)
    }
}
