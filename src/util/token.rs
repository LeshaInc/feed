use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct AuthToken {
    pub iss: String,
    pub sub: String,
    pub exp: i64,
    pub id: i32,
}
