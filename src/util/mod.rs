mod token;

use rand::Rng;

pub use self::token::AuthToken;

pub fn hash_password(salt: &[u8], password: &str) -> [u8; 24] {
    let mut hash = [0; 24];
    crypto::bcrypt::bcrypt(8, salt, password.as_bytes(), &mut hash);
    hash
}

pub fn generate_salt() -> [u8; 16] {
    rand::thread_rng().gen()
}
