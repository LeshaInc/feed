use chrono::NaiveDateTime;
use regex::Regex;
use serde_derive::Serialize;

use crate::schema::*;

lazy_static! {
    pub static ref USERNAME_RE: Regex = Regex::new(r"^[0-9A-Za-z_\-]{3,20}$").unwrap();
}

#[derive(Debug, Identifiable, PartialEq, Queryable, Serialize)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub created: NaiveDateTime,
}

#[derive(Debug, Insertable, PartialEq)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
}

#[derive(Associations, Debug, Identifiable, PartialEq, Queryable)]
#[belongs_to(User)]
#[primary_key(user_id)]
pub struct Credential {
    pub user_id: i32,
    pub password: Vec<u8>,
    pub salt: Vec<u8>,
}

#[derive(Debug, Insertable, PartialEq)]
#[table_name = "credentials"]
pub struct NewCredential<'a> {
    pub user_id: i32,
    pub password: &'a [u8],
    pub salt: &'a [u8],
}

#[derive(Associations, Debug, Identifiable, PartialEq, Queryable, Serialize)]
#[belongs_to(User)]
pub struct Post {
    pub id: i32,
    pub user_id: i32,
    pub body: String,
    pub created: NaiveDateTime,
}

#[derive(Debug, Insertable, PartialEq)]
#[table_name = "posts"]
pub struct NewPost<'a> {
    pub user_id: i32,
    pub body: &'a str,
}

#[derive(Associations, Debug, Identifiable, PartialEq, Queryable)]
#[belongs_to(User)]
#[belongs_to(Post)]
pub struct Like {
    pub id: i32,
    pub user_id: i32,
    pub post_id: i32,
    pub created: NaiveDateTime,
}

#[derive(Associations, Debug, Identifiable, PartialEq, Queryable, Serialize)]
#[belongs_to(User)]
#[belongs_to(Post)]
pub struct Comment {
    pub id: i32,
    pub user_id: i32,
    pub post_id: i32,
    pub body: String,
    pub created: NaiveDateTime,
}
