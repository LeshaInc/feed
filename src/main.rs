#[macro_use]
extern crate diesel;

#[macro_use]
extern crate lazy_static;

mod app;
mod config;
mod db;
mod error;
mod middlewares;
mod models;
mod resources;
mod schema;
mod util;

use actix::prelude::*;
use actix_web::server::HttpServer;
use failure::{Fallible, ResultExt};

fn main_inner() -> Fallible<()> {
    let sys = actix::System::new("feed");

    let config = config::load().context("failed to load config")?;
    let db = db::DbExecutor::new(&config).context("failed to establish database connection")?;
    let db = SyncArbiter::start(3, move || db.clone());

    let state = app::AppState {
        db,
        config: config.clone(),
    };

    HttpServer::new(move || app::create(state.clone()))
        .bind(config.server_addr)
        .context("failed to bind socket")?
        .start();

    println!("Started http server on {}", config.server_addr);

    sys.run();

    Ok(())
}

fn main() {
    let code = match main_inner() {
        Err(e) => {
            eprintln!("error: {}", e);

            for cause in e.iter_causes() {
                eprintln!("     | {}", cause);
            }

            if let Some(backtrace) = e.as_fail().backtrace() {
                eprintln!("{}", backtrace);
            }

            1
        }

        _ => 0,
    };

    std::process::exit(code);
}
